#include <iostream>
using namespace std;
class Time
{
private:
	int hour;
	int minute;
	int seconds;

public:
	Time(int h = 0, int m = 0, int s = 0)
	{
		hour = h;
		minute = m;
		seconds = s;
	}
	Time add(Time t)
	{
		Time temp;
		temp.minute = minute + t.minute;
		temp.hour = hour + t.hour;
		temp.seconds = seconds + t.seconds;
		if (temp.minute >= 60)
		{
			temp.hour++;
			temp.minute -= 60;
		}
		if (temp.seconds >= 60)
		{
			temp.minute++;
			temp.seconds -= 60;
		}
		return temp;
	}
	void display()
	{
		cout << hour << ":" << minute << ":" << seconds << endl;
	}
};
int main()
{
	Time t1(4, 30, 50);
	Time t2(5, 30, 20);
	Time t3 = t1.add(t2);

	t1.display();
	t2.display();
	t3.display();

	return 0;
}