#include <iostream>
using namespace std;

class Solid
{
private:
	float sp_vol, cube_vol, cyl_vol, rect_vol;

public:
	void volume(int a)
	{
		cube_vol = a * a * a;
	}
	void volume(double a)
	{
		sp_vol = (4 / 3) * 3.14 * a * a * a;
	}
	void volume(int a, int b, int c)
	{
		rect_vol = a * b * c;
	}
	void volume(double h, double r)
	{
		cyl_vol = 3.14 * h * r * r;
	}
	void display()
	{
		cout << "Volume of cube = " << cube_vol << endl;
		cout << "Volume of sphere = " << sp_vol << endl;
		cout << "Volume of cylinder = " << cyl_vol << endl;
		cout << "Volume of cuboid = " << rect_vol << endl;
	}
};
int main()
{
	Solid S1;

	S1.volume(8);
	S1.volume(6, 8, 4);
	S1.volume(10.0);
	S1.volume(8.0, 6.0);

	S1.display();
	return 0;
}