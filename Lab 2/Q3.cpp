#include <iostream>
using namespace std;

class Counter
{
private:
	int count;

public:
	Counter()
	{
		count = 0;
	}

	void increment()
	{
		count += 1;
	}
	void display()
	{
		cout << "Count is " << count << endl
			 << endl;
	}
	void restart()
	{
		count = 0;
	}
};
int main(int argc, char const *argv[])
{
	Counter c;

	int x;
	while (1)
	{
		cout << "Chose option (0 to quit)\n";
		cout << "1. Increment\n";
		cout << "2. Display Count\n";
		cout << "3. Restart \n\n";
		cin >> x;
		if (x == 1)
		{
			c.increment();
		}
		else if (x == 2)
		{
			c.display();
		}
		else if (x == 3)
		{
			c.restart();
		}
		else if (x == 0)
		{
			break;
		}
		else
		{
			cout << "Select appropriate option\n";
		}
	}
	return 0;
}