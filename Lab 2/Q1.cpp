#include <iostream>
using namespace std;

class complex
{
private:
	int real;
	int imaginary;

public:
	int set_real()
	{
		return real;
	}
	int set_imginary()
	{
		return imaginary;
	}
	int get_real()
	{
		return real;
	}
	int get_imaginary()
	{
		return imaginary;
	}
	void value()
	{
		cout << "Enter Real : " << endl;
		cin >> real;
		cout << "Imaginary : " << endl;
		cin >> imaginary;
	}
	void display()
	{
		cout << real << "+" << imaginary << "i" << endl;
	}

	void display1()
	{
		cout << real << "+" << imaginary << "i" << endl;
	}
	void add(complex c1, complex c2)
	{
		real = c1.real + c2.real;
		imaginary = c1.imaginary + c2.imaginary;
	}
	void mul(complex c1, complex c2)
	{
		real = c1.real * c2.real;
		imaginary = c1.imaginary * c2.imaginary;
	}
};

int main()
{
	complex c1, c2, c3, c4;

	c1.value();
	c2.value();

	cout << "Sum of two complex numbers is ";
	c3.add(c1, c2);
	c3.display();

	cout << "Product of two complex numbers is ";
	c4.mul(c1, c2);
	c4.display1();

	return 0;
}