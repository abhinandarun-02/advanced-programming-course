#include <iostream>
using namespace std;

class Complex
{
public:
	float real;
	float img;
	Complex()
	{
		real = 0.0;
		img = 0.0;
	}
	Complex(float real, float img)
	{
		real = real;
		img = img;
	}

	Complex operator+(const Complex &obj)
	{
		Complex temp;
		temp.img = this->img + obj.img;
		temp.real = this->real + obj.real;
		return temp;
	}

	Complex operator-(const Complex &obj)
	{
		Complex temp;
		temp.img = this->img - obj.img;
		temp.real = this->real - obj.real;
		return temp;
	}
	void display()
	{
		cout << this->real << " + " << this->img << "i" << endl;
	}
};
int main()
{
	Complex a;
	cout << "Enter real and complex coefficient of the first complex number: " << endl;
	cin >> a.real;
	cin >> a.img;

	Complex b;
	cout << "Enter real and complex coefficient of the second complex number: \n"
		 << endl;
	cin >> b.real;
	cin >> b.img;

	Complex c;
	cout << "Addition Result: ";
	c = a + b;
	c.display();
	cout << "Subtraction Result: ";
	c = a - b;
	c.display();

	return 0;
}