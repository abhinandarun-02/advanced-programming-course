#include <iostream>
using namespace std;

class Rectangle
{
private:
	int length, breadth;

public:
	void display_area()
	{
		int Area;
		Area = length * breadth;
		cout << "Area is: " << Area << endl;
	}
	Rectangle()
	{
		length = 0;
		breadth = 0;
	}
	Rectangle(int x, int y)
	{
		length = x;
		breadth = y;
	}
	Rectangle(Rectangle &r)
	{
		length = r.length;
		breadth = r.breadth;
	}
	~Rectangle()
	{
		cout << "Destroyed object" << endl;
	}
};

int main()
{
	Rectangle r1;
	r1.display_area();

	Rectangle r2(10, 12);
	r2.display_area();

	Rectangle r3(5, 6);
	r3.display_area();

	return 0;
}