#include <bits/stdc++.h>
using namespace std;

int main() {
    
    int a;
    long long b;
    char c;
    float d;
    long double e;
       
    cin >> a; 
    cin >> b; 
    cin >> c; 
    cin >> d; 
    cin >> e;
    
    
    cout << a <<"\n"<< b <<"\n"<< c <<"\n";
    cout << fixed << setprecision(3) << d <<"\n";
    cout << fixed << setprecision(9) << e << "\n";
    
    return 0;
}
