#include<bits/stdc++.h>
using namespace std;


int main()
{
    int m, n; 
    cin >> m >> n;

    string arr[9] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    for (int i = m; i <= n; i++) {
        if (i > 0 && i < 10) {
            cout << arr[i - 1] << "\n";
        }
        else {
            if (i % 2 != 0)
                cout << "odd" << "\n";
            else
                cout  << "even" << "\n";
        }
    }
    return 0;
}
