#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


int main() {
    int n;
    cin >> n;

    vector<int> vect(n);
    for (int i = 0; i < n; i++) {
        cin >> vect[i];
    }
    sort(vect.begin(), vect.end());

    for (auto x: vect) cout << x << " ";
    return 0;
}
