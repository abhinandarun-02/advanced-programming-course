#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> vect(n);
    for (int i = 0; i < n; i++) {
        cin >> vect[i];
    }

    int ind;
    cin >> ind;

    vect.erase(vect.begin() + ind - 1);

    int a, b;
    cin >> a >> b;
    vect.erase(vect.begin() + a - 1, vect.begin() + b - 1);

    cout << vect.size() << endl;
    for (int x: vect) cout << x << " ";


    return 0;
}
