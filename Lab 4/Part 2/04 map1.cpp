#include <iostream>
#include <map>
#include <algorithm>

using namespace std;

map<int, pair<string, int>> map1 = {{1,  {"January",   30}},
                                    {2,  {"February",  28}},
                                    {3,  {"March",     31}},
                                    {4,  {"April",     30}},
                                    {5,  {"May",       31}},
                                    {6,  {"June",      30}},
                                    {7,  {"July",      31}},
                                    {8,  {"August",    31}},
                                    {9,  {"September", 30}},
                                    {10, {"October",   31}},
                                    {11, {"November",  30}},
                                    {12, {"December",  31}}};

// A Function to format string in Sentence case
string format_string(string str) {
    str[0] = toupper(str[0]);
    transform(str.begin() + 1, str.end(), str.begin() + 1, ::tolower);
    return str;
}


int main() {

    string input;
    cin >> input;

    string key = format_string(input); //Formatting input

    int index = 0;
    bool flag = false;

    // Traversing map1 to find the key of input string
    for (int i = 1; i <= 12; i++) {
        if (map1[i].first == key) {
            index = i;
            flag = true;
            break;
        }
    }

    // Exiting program when wrong input is provided
    if (!flag) {
        cout << "Invalid Choice" << endl;
        return 0;
    }

    // days of input month
    cout << map1[index].second << " days" << endl;

    // Next Month
    if ((index + 1) <= 12) {
        cout << map1[index + 1].first << endl;
        cout << map1[index + 1].second << " days" << endl;
    }

    // Previous Month
    if ((index - 1) >= 1) {
        cout << map1[index - 1].first << endl;
        cout << map1[index - 1].second << " days" << endl;
    }


    return 0;
}