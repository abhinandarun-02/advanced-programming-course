#include <iostream>
#include <set>

using namespace std;


int main() {

    set<int> set1;

    int q, x, y;
    cin >> q;

    while (q--) {

        cin >> y;
        cin >> x;

        switch (y) {
            case 1:
                set1.insert(x);
                break;
            case 2:
                set1.erase(x);
                break;
            case 3:
                auto it = set1.find(x);
                cout << ((it != set1.end()) ? "Yes" : "No") << endl;
                break;
        }

    }
    return 0;
}



