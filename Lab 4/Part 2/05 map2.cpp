#include <iostream>
#include <map>

using namespace std;

void name_asc(map<string, int> map_asc) {
    cout << "Ascending Order by Name\n";
    for (auto itr = map_asc.begin(); itr != map_asc.end(); itr++) {
        cout << itr->first << " " << itr->second << endl;
    }
}

void name_dec(map<string, int> map_dec) {
    cout << "\nDescending Order by Name\n";
    for (auto itr = map_dec.rbegin(); itr != map_dec.rend(); itr++) {
        cout << itr->first << " " << itr->second << endl;
    }
}

void roll_asc(map<int, string> map_asc) {
    cout << "\nAscending Order by Roll No\n";
    for (auto itr = map_asc.begin(); itr != map_asc.end(); itr++) {
        cout << itr->first << " " << itr->second << endl;
    }
}

void roll_dec(map<int, string> map_dec) {
    cout << "\nDescending Order by Roll No\n";
    for (auto itr = map_dec.rbegin(); itr != map_dec.rend(); itr++) {
        cout << itr->first << " " << itr->second << endl;
    }
}


int main() {

    map<string, int> name = {{"Abhinand",      221},
                             {"Parthasarathi", 250},
                             {"Aswin",         210},
                             {"Akash",         227},
                             {"Prabath",       234},
                             {"Vani",          243},
                             {"Rahul",         260}};

    name_asc(name);
    name_dec(name);

    map<int, string> roll_no;
    for (auto itr = name.begin(); itr != name.end(); itr++) {
        roll_no.insert(make_pair(itr->second, itr->first));
    }

    roll_asc(roll_no);
    roll_dec(roll_no);
    return 0;
}

