#include <iostream>
#include <iosfwd>
#include <string>
#include <sstream>
#include <map>

using namespace std;

map<string, int> map1;

void wordsCount(string str) {
    istringstream wordStream(str);
    string word;
    while (wordStream >> word) {
        map1[word]++;
    }
}

int main() {

    cout << "Enter a Sentence :";
    string str;
    getline(cin, str);

    wordsCount(str);

    cout << "No of Words = " << map1.size() << endl << endl;
    for (auto &itr: map1) {
        cout << itr.first << " -> " << itr.second << endl;
    }

    return 0;
}
