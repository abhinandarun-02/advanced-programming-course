#include <iostream>
#include <set>

using namespace std;

int main() {

    multiset<int> multiset;

    cout << "Enter no of Iterations : ";
    int n;
    cin >> n;

    while (n--) {

        int choice, num;
        cout << "1 -> Insert \t 2 -> Search\n";
        cout << "Enter your Choice : ";
        cin >> choice;

        switch (choice) {
            case 1:
                cout << "Enter the No : ";
                cin >> num;
                multiset.insert(num);
                cout << "Number " << num << " inserted successfully\n" << endl;
                break;

            case 2:
                cout << "Enter the No : ";
                cin >> num;
                if (multiset.count(num) >= 1)
                    cout << num << " found " << multiset.count(num) << " times in the array\n" << endl;
                else
                    cout << num << " not found\n" << endl;
                break;

            default:
                cout << "Wrong Choice" << endl;
        }
    }

    return 0;
}
