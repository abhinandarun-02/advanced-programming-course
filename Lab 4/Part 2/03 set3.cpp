#include <iostream>
#include <vector>
#include <set>

using namespace std;

int main() {

    set<pair<int, int>> set1 = {{1,  9},
                                {10, 20},
                                {21, 40},
                                {41, 60},
                                {61, 80},
                                {81, 100}};

    vector<int> age = {1, 30, 25, 60, 75, 41};


    for (auto it: set1) {
        if (age[0] >= it.first && age[0] <= it.second) {
            cout << "[Age : " << age[0] << "] falls in " << it.first << "-" << it.second << endl;
            break;
        }
    }

    for (auto it: set1) {
        if (age[1] >= it.first && age[1] <= it.second) {
            cout << "[Age : " << age[1] << "] falls in " << it.first << "-" << it.second << endl;
            break;
        }
    }

    cout << endl;

    for (auto it = set1.begin(); it != set1.end(); it++) {
        int count = 0;
        for (int x: age) {
            if (x >= it->first && x <= it->second)
                count++;
        }
        cout << "No of People in " << it->first << "-" << it->second << " = " << count << endl;
    }
    return 0;
}