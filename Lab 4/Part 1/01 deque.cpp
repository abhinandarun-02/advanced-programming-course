#include <iostream>
#include <deque>
using namespace std;

int main()
{
    deque<int> deq{1, 2, 3, 4, 5};
    deq.push_back(6);
    deq.push_front(7);
    deq.pop_back();
    deq.pop_front();

}
