#include <iostream>
#include <stack>
using namespace std;

void sort_Stack(stack<int> &Stack)
{
    stack<int> tempStack;
    int temp, temp1;

    while (!Stack.empty())
    {
        temp = Stack.top();
        Stack.pop();
        if (tempStack.empty() || temp > tempStack.top())
        {
            tempStack.push(temp);
        }
        else
        {
            while (!tempStack.empty() && temp < tempStack.top())
            {
                temp1 = tempStack.top();
                tempStack.pop();
                Stack.push(temp1);
            }
            tempStack.push(temp);
        }
    }

    while (!tempStack.empty())
    {
        temp = tempStack.top();
        tempStack.pop();
        Stack.push(temp);
    }
}

int main()
{
    int size, num;
    stack<int> Stack;

    cout << "Enter the no elements :  ";
    cin >> size;

    cout << "\nEnter " << size << " Elements : ";
    while (size--)
    {
        cin >> num;
        Stack.push(num);
    }

    sort_Stack(Stack);

    cout << "\nSorted Elements are : ";
    int temp = Stack.top();
    Stack.pop();
    cout << temp;
    while (!Stack.empty())
    {
        temp = Stack.top();
        Stack.pop();
        cout << ", " << temp;
    }

    return 0;
}