#include <iostream>
#include <queue>
typedef long long ll;
using namespace std;

int main()
{

    ll tc;
    cin >> tc;
    while (tc--)
    {
        ll n, z;
        cin >> n >> z;

        priority_queue<ll> que;

        ll num;
        for (ll i = 1; i <= n; i++)
        {
            cin >> num;
            que.push(num);
        }

        ll times = 0;
        ll top = 0;
        bool flag = true;
        while (z > 0 && flag == true)
        {
            top = que.top();
            if (top <= 0)
                flag = false;
            z -= top;
            que.pop();
            que.push(top / 2);
            times++;
        }

        if (flag)
            cout << times << endl;
        else
            cout << "Evacuate" << endl;
    }

    return 0;
}