#include <iostream>
#include <array>
using namespace std;

class Queue
{
private:
    int head;
    int rear;
    int count;
    static const int SIZE = 10;
    array<int, SIZE> arr;

public:
    Queue()
    {
        head = -1;
        rear = -1;
        count = 0;
    }

    bool empty()
    {
        return count == 0;
    }

    int size()
    {
        return count;
    }

    void front()
    {
        if (count < 1)
            cout << "\nQueue is empty";
        else
            cout << "\nThe Front Element is : " << arr[head];
    }

    void back()
    {
        if (count < 1)
            cout << "\nQueue is empty";
        else
            cout << "\nThe Back Element is : " << arr[rear];
    }

    void push_back(int num)
    {
        if (head == -1 && rear == -1)
        {
            head = rear = 0;
            arr.at(rear) = num;
            count++;
        }
        else if (rear != arr.size() - 1)
        {
            rear++;
            arr.at(rear) = num;
            count++;
        }
        else
        {
            cout << "Error in insertion!" << endl;
        }
    }

    void pop_front()
    {
        if (head == -1 && rear == -1)
        {
            cout << "Queue is Empty!" << endl;
        }
        else if (head == rear)
        {
            arr.at(head) = 0;
            head = rear = -1;
            count--;
        }
        else
        {
            arr.at(head) = 0;
            head++;
            count--;
        }
    }
};

int main()
{

    Queue que1;
    int value, choice;

    while (true)
    {
        cout << "\n\n1. Insertion\n2. Deletion\n3. Front Element\n4. Back Element\n5. Is_Empty\n6. Size\n7. Exit";
        cout << "\nEnter your choice: ";
        cin >> choice;

        switch (choice)
        {

        case 1:
            cout << "\nEnter the value to be insert: ";
            cin >> value;
            que1.push_back(value);
            break;

        case 2:
            que1.pop_front();
            break;

        case 3:
            que1.front();
            break;

        case 4:
            que1.back();
            break;

        case 5:
            if (que1.empty())
                cout << "\nTrue";
            else
                cout << "\nFalse";
            break;

        case 6:
            cout << "\nCurrent Size : " << que1.size();
            break;

        case 7:
            exit(0);

        default:
            cout << "\nTry Again!";
        }
    }
    return 0;
}