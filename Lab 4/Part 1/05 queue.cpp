#include <iostream>
#include <queue>
using namespace std;

int main()
{
    queue<int> que1, que2;

    if (que1.empty())
        cout << "Queue is Empty" << endl;

    else
        cout << "Queue is not Empty" << endl;

    que1.emplace(3);
    que1.emplace(6);
    que1.emplace(7);

    que1.swap(que2);

    int front;
    while (!que1.empty())
    {
        front = que2.front();
        que2.pop();
        cout << front << " ";
    }

    que1.push(10);
    que1.push(20);
    que2.push(30);
    que2.push(40);
    que2.push(50);

    que1.swap(que2);

    for (int i = 0; i < que1.size(); i++)
    {
        front = que1.front();
        que1.pop();
        que2.push(front);
        que1.push(front);
    }

    cout << "\nQueue 1 Size : " << que1.size();
    cout << "\nQueue 2 Size : " << que2.size();

    return 0;
}