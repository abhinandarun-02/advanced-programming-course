#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
using namespace std;

int main()
{
    bool flag;
    vector<int> marks{20, 25, 25, 50, 40, 22, 25, 45, 42, 44, 33, 35, 44, 48};

    flag = true;
    for (int x : marks)
    {
        if (x % 2 == 0)
        {
            flag = false;
            break;
        }
    }
    cout << (flag ? "All Marks are Odd" : "Not All Marks are Odd");

    flag = true;
    for (int x : marks)
    {
        if (x % 2 != 0)
        {
            flag = false;
            break;
        }
    }
    cout << (flag ? "\nAll Marks are Even" : "\nNot All Marks are Even");

    flag = true;
    for (int x : marks)
    {
        if (x <= 20)
        {
            flag = false;
            break;
        }
    }
    cout << (flag ? "\nAll Marks are above 20" : "\nNot All Marks are above 20");

    flag = true;
    for (int x : marks)
    {
        if (x == 50)
        {
            flag = false;
            break;
        }
    }
    cout << (!flag ? "\nSome Students got 50" : "\nNobody got 50");

    flag = true;
    for (int x : marks)
    {
        if (x < 0)
        {
            flag = false;
            break;
        }
    }
    cout << (!flag ? "\nSome Students got Negative Mark" : "\nNobody got Negative Mark");

    cout << "\nMarks after adding Attendance Marks : ";
    for (int x : marks)
    {
        cout << x + 5 << " ";
    }

    cout << "\nIndices with Mark 44 are : ";
    for (int i = 0; i < marks.size(); i++)
    {
        if (marks[i] == 44)
            cout << i << " ";
    }

    cout << "\nThe Average Mark is ";
    float avg = accumulate(marks.begin(), marks.end(), 0.0) / marks.size();
    cout << avg;

    cout << "\nSorting Marks...";
    sort(marks.begin(), marks.end());

    cout << "\nAdjacent Difference : ";
    for (int i = 1; i < marks.size(); i++)
    {
        cout << marks[i] - marks[i - 1] << " ";
    }

    cout << "\nMerging Marks...";
    vector<int> marks2{44, 43, 25, 39, 33, 38, 30, 44, 35, 22, 38, 49, 33, 41};
    for (int x : marks2)
        marks.push_back(x);

    return 0;
}