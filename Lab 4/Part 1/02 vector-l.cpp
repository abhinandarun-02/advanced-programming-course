#include <iostream>
#include <array>
#include <algorithm>
using namespace std;

int main()
{

    array<int, 4> arr{{1, 3, 2, 5}};

    sort(arr.begin(), arr.end());

    cout << "The 24 possible arrays are : \n";

    do
    {
        for (int x : arr)
            cout << x << " ";
        cout << endl;
    } while (next_permutation(arr.begin(), arr.end()));

    return 0;
}