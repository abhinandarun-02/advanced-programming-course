#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<string> buildArray(vector<int> &target, int n)
    {
        vector<string> res;
        int j = 0;
        for (int i = 1; i <= n; i++)
        {
            res.push_back("Push");
            if (target[j] != i)
                res.push_back("Pop");
            else
                j++;

            if (j == target.size())
                break;
        }

        return res;
    }
};

int main()
{
    Solution sol;

    vector<int> target{2, 3, 4};
    int n = 4;

    vector<string> res = sol.buildArray(target, n);

    cout << "[ ";
    for (auto x : res)
        cout << "\"" << x << "\" ";
    cout << "]" << endl;
    return 0;
}