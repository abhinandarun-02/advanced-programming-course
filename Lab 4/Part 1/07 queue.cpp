#include <iostream>
#include <vector>
#include <queue>
using namespace std;

void print_vector(vector<int> vect)
{
    cout << "\nMinimum Vector is [ ";
    for (auto x : vect)
        cout << x << " ";
    cout << "]";
    cout << endl;
}

int main()
{

    priority_queue<vector<int>, vector<vector<int>>, greater<vector<int>>> que;

    int n, num, nv;
    cout << "Enter no of Vectors : ";
    cin >> nv;
    cout << "\nEnter no of Elements per Vector : ";
    cin >> n;
    vector<int> input(n, 0);

    for (int i = 0; i < nv; i++)
    {
        cout << "\nType " << n << " Elements : ";
        for (int j = 0; j < n; j++)
        {
            cin >> num;
            input[j] = num;
        }
        que.push(input);
    }

    print_vector(que.top());

    return 0;
}